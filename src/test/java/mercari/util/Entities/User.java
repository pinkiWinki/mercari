package mercari.util.Entities;

/**
 * Created by Elena on 9/9/17.
 */
public class User {
    private final String email;
    private final String password;

    public User(String email, String password)
    {
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
