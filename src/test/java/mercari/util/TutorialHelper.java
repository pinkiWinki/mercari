package mercari.util;

import io.appium.java_client.android.AndroidDriver;
import mercari.page.LoginPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;

import static mercari.util.Helpers.for_find;
import static mercari.util.Helpers.setWait;

/**
 * Created by Elena on 9/9/17 .
 */
public class TutorialHelper {

    public static AndroidDriver driver;
    public static URL serverAddress;
    private static WebDriverWait driverWait;

    /**
     * Initialize the webdriver. Must be called before using any helper methods. *
     */
    public static void init(AndroidDriver webDriver, URL driverServerAddress) {
        driver = webDriver;
        serverAddress = driverServerAddress;
        int timeoutInSeconds = 60;
        // must wait at least 60 seconds for running on Sauce.
        // waiting for 30 seconds works locally however it fails on Sauce.
        driverWait = new WebDriverWait(webDriver, timeoutInSeconds);
    }

    public static void skipTutorial(String endButtonTag)
    {
        setWait(0);
        while (Helpers.driver.findElements(for_find(endButtonTag)).isEmpty()) {
            continueClick();
        }
        setWait(30);
        startClick(endButtonTag);
    }

    /** Verify the home page has loaded.
     *  Click the continue button.*/
    private static void continueClick() {
        Helpers.find("次へ").click();
    }

    /**
     *  Click the continue button.*/
    private static void startClick(String endButtonTag) {
        Helpers.find(endButtonTag).click();
    }
}
