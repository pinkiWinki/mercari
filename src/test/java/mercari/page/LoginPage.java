package mercari.page;

import mercari.util.Entities.User;
import org.openqa.selenium.WebElement;

import static mercari.util.Helpers.*;

/**
 * Created by elenita on 9/9/17.
 */
public class LoginPage {

    public static final String SKIP = "スキップ";
    public static final String LOGIN = "ログイン";
    public static final String USE_MAIL_OR_PHONE = "メールまたは電話番号でログイン";
    public static final String MAIL_OR_PHONE = "メールまたは電話番号";
    public static final String PASSWORD = "パスワード";

    /** Verify the login page has loaded **/
    public static WebElement loaded() {
        return find(SKIP);
    }

    public ListPage skip() {
         find(SKIP).click();
        return new ListPage();
    }

    public ListPage LoginByEmail(User user){
        find(LOGIN).click();
        find(USE_MAIL_OR_PHONE).click();
        find(MAIL_OR_PHONE).sendKeys(user.getEmail());
        find(PASSWORD).click();
        find(PASSWORD).sendKeys(user.getPassword());

        tags("android.widget.Button").get(0).click();
        return new ListPage();
    }
}
