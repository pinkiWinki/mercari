package mercari.page;

import static mercari.util.Helpers.find;

/**
 * Created by elenita on 9/9/17.
 */
public class PurchasePage {

    public PurchasePage loaded() {

        find("購入する");
        return this;
    }

    public void buyItemDefaultPayment()
    {
        /* at this page you can purchase the item, you should also check the payment method because I don't have a test
         environment and I don't want to purchase something, lets assume that we have a payment method by default
         and clicking this button we proceed to buy an item.
          */
        find("購入する").click();
    }
}
