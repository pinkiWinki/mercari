package mercari.page;

import mercari.util.TutorialHelper;

import static mercari.util.Helpers.find;

/**
 * Created by elenita on 9/9/17.
 */
public class PurchaseTutorialPage {

    public static final String NEXT = "次へ";
    public static final String TO_PURCHASE_SCREEN = "購入画面へ";

    public PurchaseTutorialPage loaded() {

        find(NEXT);
        return this;
    }

    public PurchasePage skipPaymentTutorial()
    {
        TutorialHelper.skipTutorial(TO_PURCHASE_SCREEN);
        return new PurchasePage();
    }
}
