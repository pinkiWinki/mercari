package mercari.page;

import mercari.util.AppiumTest;

import static mercari.util.Helpers.*;

/**
 * Created by elenita on 9/9/17.
 */
public class ItemPage {


    public static final String PRODUCT_DESCRIPTION = "商品の説明";
    public static final String TO_PURCHASE = "購入手続きへ";
    public static final String COMMENT = "コメント";

    public ItemPage loaded() {

        find(PRODUCT_DESCRIPTION);
        return this;
    }

    public PurchaseTutorialPage buyItemFirstTime()
    {
        find(TO_PURCHASE).click();
        return new PurchaseTutorialPage();
    }

    public CommentPage openCommentPage()
    {
        find(COMMENT).click();
        return new CommentPage();
    }


}
