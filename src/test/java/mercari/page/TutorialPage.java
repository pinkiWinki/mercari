package mercari.page;

import mercari.util.TutorialHelper;
import org.openqa.selenium.WebElement;

import static mercari.util.Helpers.*;

/**
 * Created by Elena on 9/9/17.
 */
public class TutorialPage {

    public static final String NEXT = "次へ";
    public static final String LETS_START = "さあ、はじめよう！";

    /**
     * Verify the tutorial page has loaded *
     */
    public WebElement loaded() {
        return find(NEXT);
    }

    public LoginPage loginPage() {
        loaded();
        TutorialHelper.skipTutorial(LETS_START);
        return new LoginPage();

    }
}
