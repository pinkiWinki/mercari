package mercari.page;

import mercari.util.AppiumTest;
import org.openqa.selenium.WebElement;

import java.util.List;

import static mercari.util.Helpers.for_text;
import static mercari.util.Helpers.*;

/**
 * Created by elenita on 9/9/17.
 */
public class ListPage extends AppiumTest{

    public static final int IMAGE_LIST_START = 5;
    public static final String ALL = "すべて";

    public ListPage loaded() {

        wait(for_text(ALL));
        return new ListPage();
    }

    public ListPage loaded(String section) {

        wait(for_text(section));
        return new ListPage();
    }

    public WebElement allElement()
    {
        return find(ALL);
    }

    public ItemPage clickProductAtPosition(int index)
    {
        List<WebElement> products  = tags("android.widget.ImageView");

        products.get(index + IMAGE_LIST_START).click();

        return new ItemPage();

    }

    public  ListPage clickOnSection(String section)
    {
        // here move horizontal, clicking in the buttons
        // TODO add tries so in case of not found, it does not get stack.
        setWait(0);
        boolean sect = false;

        for(WebElement element : tags("android.widget.TextView"))
        {
            if(element.getText().contains(section))
            {
                element.click();
                sect = true;
            }
        }
        if(!sect)
        {
            swipeRight();
            clickOnSection(section);
        }

        setWait(30);
        return loaded(section);
    }
}
