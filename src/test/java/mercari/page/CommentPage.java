package mercari.page;

import org.openqa.selenium.WebElement;

import java.util.SortedSet;
import java.util.TreeSet;

import static mercari.util.Helpers.*;

/**
 * Created by elenita on 10/9/17.
 */
public class CommentPage {

    public static final String コメント = "コメント";

    public CommentPage loaded()
    {
        find(コメント);
        return this;
    }

    public SortedSet<String> getComments()
    {
        SortedSet<String> comments = new TreeSet<String>();

        for(WebElement element : elements(for_text(2)))
        {
            comments.add(element.getText());
        }

        // scroll down in order to catch all comments
        getMoreComments(comments);

        return comments;
    }

    public void setComment(String comment)
    {
        element(for_tags("android.widget.EditText")).sendKeys(comment);
        element(for_tags("android.widget.ImageView")).click();

    }

    private SortedSet<String> getMoreComments(SortedSet<String> comments)
    {
        scroll();
        int newComments = 0;
        for(WebElement element : elements(for_text(2)))
        {
            if(!comments.contains(element.getText()))
            {
                comments.add(element.getText());
                newComments++;
            }
        }

        if(newComments > 0)
        {
            getMoreComments(comments);
        }

        return comments;
    }
}
