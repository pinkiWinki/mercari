package mercari.test;

import mercari.page.TutorialPage;
import mercari.util.AppiumTest;
import mercari.util.Data.Users;

public class MercariListTest extends AppiumTest {

    public static final String MEN = "メンズ";

    @org.junit.Test
    public void buyItemTest() throws Exception {

        new TutorialPage()
                .loginPage()
                .LoginByEmail(Users.elenaUser).loaded()
                .clickProductAtPosition(1)
                .buyItemFirstTime()
                .skipPaymentTutorial()
                .buyItemDefaultPayment();

        // Add assertion
    }

    @org.junit.Test
    public void setCommentTest() throws Exception {

        new TutorialPage().loginPage()
                .skip()
                .clickProductAtPosition(1)
                .openCommentPage().setComment("");

        // Add assertion
    }

    @org.junit.Test
    public void ClickOnSpecificList() {
        new TutorialPage().loginPage()
                .skip().loaded()
                .clickOnSection(MEN);

        // Add assertion
    }

    @org.junit.Test
    public void getComments() {
        new TutorialPage().loginPage()
                .skip().loaded()
                .clickProductAtPosition(1)
                .openCommentPage().getComments();

        // Add assertion
    }
}